﻿using System;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using System.Collections.Generic;

namespace Phonewordproject
{
    [Activity(Label = "PhonewordProject", MainLauncher = true)]
    public class MainActivity : Activity
    {
        private List<KeyValuePair<string, string>> planets;


        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Set our view from the "Main" layout resource
            SetContentView(Resource.Layout.Main);

            //Button button = FindViewById<Button>(Resource.Id.myButton);
            EditText et1 = FindViewById<EditText>(Resource.Id.edittext);
            String numbe = ((EditText)et1).ToString();

            Spinner spinner = FindViewById<Spinner>(Resource.Id.spinner);
            planets = new List<KeyValuePair<string, string>>
     {
    new KeyValuePair<string, string>("Botswana", "267"),
    new KeyValuePair<string, string>("Brazil", "55"),
    new KeyValuePair<string, string>("British Indian Ocean Territory", "246"),
    new KeyValuePair<string, string>("British Virgin Islands", "1"),
    new KeyValuePair<string, string>("Afghanistan" , "93"),
    new KeyValuePair<string, string>("Bangladesh", "880"),
    new KeyValuePair<string, string>("Canada", "1"),
    new KeyValuePair<string, string>("Iran", "98"),
    new KeyValuePair<string, string>("Israel","972"),
    new KeyValuePair<string, string>("Malaysia","60"),
    new KeyValuePair<string, string>("Maldives","960"),
    new KeyValuePair<string, string>("New Zealand","64"),
    new KeyValuePair<string, string>("Pakistan","92")

};




            List<string> planetNames = new List<string>();
            foreach (var item in planets)
                planetNames.Add(item.Key);


            spinner.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs>(spinner_ItemSelected);
            var adapter = new ArrayAdapter<string>(this,
    Android.Resource.Layout.SimpleSpinnerItem, planetNames);
            adapter.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);
            spinner.Adapter = adapter;

            
        }
        
        private void spinner_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
        {
           
            Spinner spinner = (Spinner)sender;
            var numbe= FindViewById<EditText>(Resource.Id.edittext).Text;
            // String number = editText.ToString();
            string toast = string.Format("The mean temperature for planet {0} is {1}",
                spinner.GetItemAtPosition(e.Position), planets[e.Position].Value);
            var uri = Android.Net.Uri.Parse("tel:"+planets[e.Position].Value+numbe);
            var intent = new Intent(Intent.ActionDial, uri);
            StartActivity(intent);
            // Toast.MakeText(this, toast, ToastLength.Long).Show();
}
           
}

}


